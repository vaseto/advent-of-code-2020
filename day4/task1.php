<?php

include_once '../bootstrap.php';

$inputLinesRaw = readFileByLines(__DIR__ . DIRECTORY_SEPARATOR . 'task-input.txt');

/**
 * Group the sporadic line into single passport
 */
$passports = [];
$passportIndex = 0;
foreach ($inputLinesRaw as $eachLine) {
    if (!array_key_exists($passportIndex, $passports)) {
        $passports[$passportIndex] = [];
    }

    // Remove whitespace - this will clearly separate lines, even if whitespace is available on a single line
    $eachLine = trim($eachLine);
    if (strlen($eachLine) == 0) {
        $passportIndex++;
    } else {
        // This is an actual passport line - move it to the current passport
        $linePairs = explode(' ', $eachLine);
        foreach ($linePairs as $eachPair) {
            // Get the pairs
            list($key, $value) = explode(':', $eachPair);
            // inject them in the current passport
            $passports[$passportIndex][trim($key)] = trim($value);
        }
    }
}

$mandatoryFields = [
    'byr', // (Birth Year)
    'iyr', // (Issue Year)
    'eyr', // (Expiration Year)
    'hgt', // (Height)
    'hcl', // (Hair Color)
    'ecl', // (Eye Color)
    'pid', // (Passport ID)
];

$mandatoryFieldsNum = count($mandatoryFields);

$optionalField = [
    'cid', //(Country ID)
];

$validPassports = 0;
foreach ($passports as $eachPassport) {
    if (count($eachPassport) < $mandatoryFieldsNum) {
        continue;
    }

    // Remove the optional field
    if (isset($eachPassport['cid'])) {
        unset($eachPassport['cid']);
    }

    $passportFields = array_keys($eachPassport);

    // All fields are present
    if (array_diff($passportFields, $mandatoryFields) == []
        && array_diff($mandatoryFields, $passportFields) == []) {
         $validPassports++;
    }
}

print "Total passports: " . count($passports) . "\n";
print "Total valid passports: " . $validPassports . "\n\n";



