<?php

include_once '../bootstrap.php';

$inputLinesRaw = readFileByLines(__DIR__ . DIRECTORY_SEPARATOR . 'task-input.txt');

/**
 * Group the sporadic line into single passport
 */
$passports = [];
$passportIndex = 0;
foreach ($inputLinesRaw as $eachLine) {
    if (!array_key_exists($passportIndex, $passports)) {
        $passports[$passportIndex] = [];
    }

    // Remove whitespace - this will clearly separate lines, even if whitespace is available on a single line
    $eachLine = trim($eachLine);
    if (strlen($eachLine) == 0) {
        $passportIndex++;
    } else {
        // This is an actual passport line - move it to the current passport
        $linePairs = explode(' ', $eachLine);
        foreach ($linePairs as $eachPair) {
            // Get the pairs
            list($key, $value) = explode(':', $eachPair);
            // inject them in the current passport
            $passports[$passportIndex][trim($key)] = trim($value);
        }
    }
}

$mandatoryFields = [
    // Passport IDs
    'pid' => ['type' => 'pid'], // (Passport ID)
    // Hex Color
    'hcl' => ['type' => 'colour'], // (Hair Color)
    // Eye Color
    'ecl' => [
        'type' => 'eyes',
        'rules' => ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
    ], // (Eye Color - one of options)
    // Years
    'byr' => [
        'type' => 'year',
        'rules'=> [
            'min' => 1920,
            'max' => 2002
        ]
    ], // (Birth Year)
    'iyr' => [
        'type' => 'year',
        'rules'=> [
            'min' => 2010,
            'max' => 2020
        ]
    ], // (Issue Year)
    'eyr' => [
        'type' => 'year',
        'rules'=> [
            'min' => 2020,
            'max' => 2030
        ]
    ], // (Expiration Year)
    // Height
    'hgt' => [
        'type' => 'height',
        'rules' => [
            'cm' => [
                'min' => 150,
                'max' => 193
            ],
            'in' => [
                'min' => 59,
                'max' => 76
            ],
        ]
    ],
];
$mandatoryKeys = array_keys($mandatoryFields);

$mandatoryFieldsNum = count($mandatoryFields);
$totalValidPasswords = 0;

foreach ($passports as $eachPassport) {
    if (count($eachPassport) < $mandatoryFieldsNum) {
        continue;
    }

    // Remove the optional field
    if (isset($eachPassport['cid'])) {
        unset($eachPassport['cid']);
    }

    $passportFields = array_keys($eachPassport);

    // Validate if all fields are present
    if (array_diff($passportFields, $mandatoryKeys) == []
        && array_diff($mandatoryKeys, $passportFields) == []
    ) {
        // All fields are present, lets validate the data

        $isValid = true;
        foreach ($eachPassport as $fieldName => $value) {
            // Drop on the first invalid fields
            $type = $mandatoryFields[$fieldName]['type'];
            $rules = isset($mandatoryFields[$fieldName]['rules']) ? $mandatoryFields[$fieldName]['rules'] : [];

            if (!validate($mandatoryFields[$fieldName]['type'], $value, $rules)) {
                $isValid = false;
                break;
            }
        }

        if ($isValid) {
            $totalValidPasswords++;
        }
    }
}

print "Total valid passports " . $totalValidPasswords . "\n\n";

