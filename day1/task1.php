<?php

include_once '../bootstrap.php';

$inputRows = readFileByLines(__DIR__ . '/task-input.txt');

$targetSum = 2020;
$combinations = [];
$z = 0;

for ($i = 0; $i < 200; $i++) {
    for ($j = $i; $j < 200; $j++) {

        if (($inputRows[$i] + $inputRows[$j]) === $targetSum) {
            $combinations = [
                $inputRows[$i], $inputRows[$j]
            ];
        }

        $z++;

    }

}

print "Total iterations " . $z . "\n";
print "Match rows are \n";
print_r($combinations);
print "\n\n The task solution is: ";
print_r(  (int) $combinations[0] * $combinations[1] );
print "\n";



