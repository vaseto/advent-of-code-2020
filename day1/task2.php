<?php

include_once '../bootstrap.php';

$inputRows = readFileByLines(__DIR__ . '/task-input.txt');


$targetSum = 2020;
$totalIterations = 0;
$combinations = [];

for ($i = 0; $i < 200; $i++) {

    for ($j = $i; $j < 200; $j++) {

        for ($z = $j; $z < 200; $z++) {

            if (($inputRows[$i] + $inputRows[$j] + $inputRows[$z]) === $targetSum) {
                $combinations = [
                    0 => $inputRows[$i],
                    1 => $inputRows[$j],
                    2 => $inputRows[$z]
                ];
            }

            $totalIterations++;
        }

    }

}

print "Total iterations " . $totalIterations . "\n";
print "Match rows are \n";

print_r($combinations);

print "\n\n The task solution is: ";

print_r( $combinations[0] * $combinations[1] * $combinations[2] );
print "\n";



