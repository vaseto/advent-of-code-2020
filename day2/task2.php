<?php

include_once '../bootstrap.php';

$inputRows = readFileByLines(__DIR__ . '/task-input.txt');

/** */
$totalValid = 0;
for ($i = 0, $z = count($inputRows); $i < $z; $i++) {

    preg_match('/^(?P<pos1>\d*)-(?P<pos2>\d*) (?P<letter>\w{1}): (?P<password>\w*)$/i', $inputRows[$i], $rules);

    $password = $rules['password'];
    $pos1 = ((int)$rules['pos1'] - 1);
    $pos2 = ((int)$rules['pos2'] - 1);
    $letter = $rules['letter'];


    if ($password[ $pos1 ] === $letter) {
        if ($password[  $pos2 ] !== $letter) {
            $totalValid++;
        }
    } else {
        if ($password[ $pos2 ] === $letter) {
            $totalValid++;
        }
    }

}

print "Total valid passwords " . $totalValid . "\n";
