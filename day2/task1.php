<?php

include_once '../bootstrap.php';

$inputRows = readFileByLines(__DIR__ . '/task-input.txt');


/** */
$totalValid = 0;
for ($i = 0, $z = count($inputRows); $i < $z; $i++) {
    preg_match('/^(?P<min>\d*)-(?P<max>\d*) (?P<letter>\w{1}): (?P<password>\w*)$/i', $inputRows[$i], $rules);


    $letterOccurances = substr_count($rules['password'], $rules['letter']);
    if ($rules['min'] <= $letterOccurances && $rules['max'] >= $letterOccurances) {
        $totalValid++;
    }
}

print "Total valid passwords " . $totalValid . "\n";
