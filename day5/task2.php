<?php

include_once '../bootstrap.php';
include_once __DIR__ . DIRECTORY_SEPARATOR . 'decodeSeat.php';

$inputLinesRaw = readFileByLines(__DIR__ . DIRECTORY_SEPARATOR . 'task-input.txt');

print("\nTOTAL Lines taken " . count($inputLinesRaw) . "\n\n");

/**
 * Build the seats grid
 */
$seatGrid = [];
for ($i = 0; $i < 128; $i++) {
    $seatGrid[ (string) $i] = [];
    for ($j = 0; $j < 8; $j++) {
        $seatGrid[ $i ][ $j ] = true;
    }
}

$highestSeatId = 0;
$lowestSeatId = 10000;
$lowestSet = [];
$highestSet = [];

/**
 * Now that we have the grid, lets destroy it bit by bit  (i.e. remove seats taken)
 */
foreach ($inputLinesRaw as $eachRowSeatCode) {
    $seatData = decodeRowAndSeat($eachRowSeatCode);

    if ($highestSeatId < $seatData['seatId']) {
        $highestSeatId = $seatData['seatId'];
        $highestSet = $seatData;
    }

    if ($lowestSeatId > $seatData['seatId']) {
        $lowestSeatId = $seatData['seatId'];
        $lowestSet = $seatData;
    }

    unset($seatGrid[ $seatData['rowId']  ][ $seatData['columnId'] ]);
    if (count($seatGrid[ $seatData['rowId'] ]) == 0) {
        unset($seatGrid[ $seatData['rowId'] ]);
    }

}

/**
 * Clean up the front most and endmost rows
 */
foreach ($seatGrid as $i => $leftoverSeats ) {
    if ($lowestSet['rowId'] >= $i) {
        unset($seatGrid[$i]);
    }
    if ($highestSet['rowId'] <= $i) {
        unset($seatGrid[$i]);
    }
}

$seatId = 0;
foreach ($seatGrid as $rowId => $rowSet) {
    foreach($rowSet as $columnId => $anything) {
       $seatId = calculateSeatId($rowId, $columnId);
    }
}

print " YOUR SEAT IS " . $seatId . "\n\n";
