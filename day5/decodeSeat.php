<?php

/**
 * @param string $rowAndSeatCode
 * @return array
 */
function decodeRowAndSeat(string $rowAndSeatCode)
{
    $rowCode = substr($rowAndSeatCode, 0, 7);
    $columnCode = substr($rowAndSeatCode, 7);

    $rowId = decodeSeatRowCode($rowCode, 'B', 'F');
    $columnId = decodeSeatRowCode($columnCode, 'R', 'L');

    return [
        'code' => $rowAndSeatCode,
        'rowCode' => $rowCode,
        'rowId' => $rowId,
        'columnCode' => $columnCode,
        'columnId' => $columnId,
        'seatId' => calculateSeatId($rowId, $columnId),
    ];
}

/**
 * The seat code is actually a reversed binary
 *
 * @param string $columnCode
 * @return int
 */
function decodeSeatRowCode(string $columnCode, $binOne, $binZero)
{
    $columnBinId = '0b' . strtr($columnCode, [
        $binOne  => '1',
        $binZero => '0'
    ]);

    return bindec($columnBinId);
}

/**
 * Calculate the Seat Sequence
 *
 * @param int $rowId
 * @param int $seatId
 * @return int
 */
function calculateSeatId(int $rowId, int $seatId)
{
    return (int) ($rowId * 8) + $seatId;
}