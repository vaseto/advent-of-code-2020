<?php

include_once '../bootstrap.php';
include_once __DIR__ . DIRECTORY_SEPARATOR . 'decodeSeat.php';

$inputLinesRaw = readFileByLines(__DIR__ . DIRECTORY_SEPARATOR . 'task-input.txt');

$highestSeatId = 0;
$lowestSeatId = 10000;

$lowestSet = [];
$highestSet = [];

// Test Data
//
//$inputLinesRaw = [
//    'BFFFBBFRRR', // : row 70, column 7, seat ID 567.
//    'FFFBBBFRRR', //: row 14, column 7, seat ID 119.
//    'BBFFBBFRLL', // : row 102, column 4, seat ID 820.
//];

foreach ($inputLinesRaw as $eachRowSeatCode) {
    $seatData = decodeRowAndSeat($eachRowSeatCode);

    if ($highestSeatId < $seatData['seatId']) {
        $highestSeatId = $seatData['seatId'];
        $highestSet = $seatData;
    }

    if ($lowestSeatId > $seatData['seatId']) {
        $lowestSeatId = $seatData['seatId'];
        $lowestSet = $seatData;
    }

}

print "Highest SeatID is " . $highestSeatId . "\n";
print_r($highestSet);
print "\n============\n";
print "Lowest SeatID is " . $lowestSeatId . "\n";
print_r($lowestSet);
print "\n\n";