<?php

include_once "../bootstrap.php";
$inputLinesRaw = readFileByLines(__DIR__ . DIRECTORY_SEPARATOR . 'task-input.txt');

/**
 * Test Data
 */
//$inputLinesRaw = [
//'abc',
//'',
//'a',
//'b',
//'c',
//'',
//'ab',
//'ac',
//'',
//'a',
//'a',
//'a',
//'a',
//'',
//'b',
//];

array_push($inputLinesRaw, '');

/**
 * Group the answers per group
 */
$groups = [];
$groupId = 0;

$totalYesAnswers = 0;
foreach ($inputLinesRaw as $eachLine) {
    // Create a new group
    if (!array_key_exists($groupId, $groups)) {
        $groups[$groupId] = [];
        $groups[$groupId] = str_split($eachLine, 1);
    }

    // Remove whitespace - this will clearly separate lines, even if whitespace is available on a single line
    $eachLine = trim($eachLine);
    if (strlen($eachLine) == 0) {

//        print "Current group " . count($groups[$groupId]) . "\n\n";
//
        // Calculate the number of current Yes answers for the group
        $totalYesAnswers += count($groups[$groupId]);

        // ... and move to the next group
        $groupId++;
    } else {
        // Get only the shared answers
        $groups[$groupId] = array_intersect(
            $groups[$groupId],
            (array) str_split($eachLine, 1)
        );
    }
}

//print_r($groups);

print "Total groups YES answers: " . $totalYesAnswers . "\n\n";