<?php

include_once "../bootstrap.php";
$inputLinesRaw = readFileByLines(__DIR__ . DIRECTORY_SEPARATOR . 'task-input.txt');

/**
 * Test data
 */
//$inputLinesRaw = [
//'abc',
//'',
//'a',
//'b',
//'c',
//'',
//'ab',
//'ac',
//'',
//'a',
//'a',
//'a',
//'a',
//'',
//'b',
//];

array_push($inputLinesRaw, '');

/**
 * Group the answers per group
 */
$groups = [];
$groupId = 0;

$totalYesAnswers = 0;
foreach ($inputLinesRaw as $eachLine) {
    // Create a new group
    if (!array_key_exists($groupId, $groups)) {
        $groups[$groupId] = [];
    }

    // Remove whitespace - this will clearly separate lines, even if whitespace is available on a single line
    $eachLine = trim($eachLine);
    if (strlen($eachLine) == 0) {

        // Get only the unique answers in the group
        $groups[$groupId] = array_unique($groups[$groupId]);

        // Calculate the number of current Yes answers for the group
        $totalYesAnswers += count($groups[$groupId]);

        // ... and move to the next group
        $groupId++;
    } else {
        // This is an actual person line line - merge the unique answers to the current group
        $groups[$groupId] = array_merge($groups[$groupId], str_split($eachLine, 1));
    }
}

print "Total groups YES answers: " . $totalYesAnswers . "\n\n";