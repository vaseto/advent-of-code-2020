<?php

include_once '../bootstrap.php';

$inputRows = readFileByLines(__DIR__ . '/task-input.txt');

/**
 * Create a grid
 */
$grid = [];
foreach ($inputRows as $rowIndex => $eachRow) {
    $rowElements = str_split($eachRow, 1);
    // The last element is empty, throw it away
    array_pop($rowElements);

    $grid[$rowIndex] = $rowElements;
}

$columnCount = count($grid[0]);
$rowCount = count($grid);


$offsetRules = [
//   X, Y
    [1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2]
];

$allTrees = 1;
foreach ($offsetRules as $eachOffsetPair) {

    $xOffset = (int) $eachOffsetPair[0];
    $yOffset = (int) $eachOffsetPair[1];

    print "Current Offset: x => $xOffset, y => $yOffset \n\n";

    $treesHit = 0;

    $x = 0;
    $y = 0;

    do {
        if ($grid[$y][$x] == '#') {
            $treesHit++;
        }
        // Move {$yOffset} down
        $y = $y + $yOffset;

        // Move {$xOffset} right
        $x = $x + $xOffset;

        // Rotate to the beginning  when you end the pattern of a row
        if ($x >= $columnCount) {
            $x = $x - $columnCount;
        }

    } while ($y < $rowCount);

    print "Trees Hit [$xOffset, $yOffset]: "   . $treesHit . "\n\n";

    $allTrees *= $treesHit;
}

print "All trees hit " . $allTrees . "\n\n";

