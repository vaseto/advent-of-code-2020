<?php

/**
 * Include helper functions
 */
include_once __DIR__ . DIRECTORY_SEPARATOR . 'helpers.php';

/**
 * Include validation functions
 */
include_once __DIR__ . DIRECTORY_SEPARATOR . 'validation.php';